# nwndocker
Docker images for the dedicated neverwinter linux server, nwnx2 and mysql database.

## Setup and run
* Build docker image with nwn server from original linuxserver archive `docker build -t nwserver nwserver`
* Clone [hendrikgit/nwnx2-linux](git@gitlab.com:hendrikgit/nwnx2-linux.git) or the version of nwnx2-linux you want to use into nwnxbuild, build the nwnxbuild image, run nwnxbuild to compile nwnx2.
 
```bash
git -C nwnxbuild clone git@gitlab.com:hendrikgit/nwnx2-linux.git
docker build -t nwnxbuild nwnxbuild
docker run --rm --mount type=bind,source="$(pwd)"/nwnxbuild/nwnx2-linux,target=/nwnx2-linux nwnxbuild
```
* Build nwnxserver image with the compiled nwnx2 files

```bash
cp -v nwnxbuild/nwnx2-linux/build/compiled/*.so nwnxserver/nwnx2-compiled/
chmod -c 644 nwnxserver/nwnx2-compiled/*
mv -v nwnxserver/nwnx2-compiled/nwnx_odmbc_*.so nwnxserver/nwnx2-compiled/nwnx_odbc.so
docker build -t nwnxserver nwnxserver
```
* Use docker-compose to run db and nwserver with bind mounts.
  If you put a sql dump with the file ending .sql into the sql folder that file will be used to initialize the db.
  The following folders are mounted into the container:
  * database
  * hak
  * modules
  * override
  * servervault
  * tlk

```bash
cd nwnxsqlserver
docker-compose up db
docker-compose run --rm --service-ports nwserver -servername SERVERNAME -module MODULENAME
```

